# EJERCICIO 16.7. DESCARGA DE DOCUMENTOS WEB

from urllib.request import urlopen


class Robot:
    def __init__(self, url):
        self.url = url
        self.descargado = False

    def retrieve(self):  # descarga el documento de la url
        if not self.descargado:
            print(f"Descargando {self.url}")
            urls = urlopen(self.url)
            self.contenido = urls.read().decode('utf-8')  # convierte los bytes en string
            self.descargado = True
        else:
            print(f"Ya se descargó el documento de {self.url} previamente")

    def content(self):  # devuelve una cadena de caracteres con el contenido del documento descargado
        self.retrieve()
        return self.contenido

    def show(self):  # muestra en pantalla el contenido del documento descargado
        print(f"Contenido del documento descargado: {self.content()}")


class Cache:
    def __init__(self):
        self.cache = {}  # diccionario vacío

    def retrieve(self, url):  # descarga el documento correspondiente a esa url
        if url not in self.cache:
            self.cache[url] = Robot(url)
        else:
            print("Ya ha sido descargado previamente")

    def content(self, url):  # devuelve una cadena de caracteres con el contenido del documento correspndiente a la url
        self.retrieve(url)
        return self.cache[url].content()

    def show(self, url):  # muestra en pantalla el contenido del documento correspondiente con esa url
        print(f"Contenido del documento correspondiente a la url: {self.content(url)}")

    def show_all(self):  # muestra un listado de todas las urls cuyo documento se ha descargado ya
        for url in self.cache:
            print(f"Url cuyo documento ya se ha descargado: {url}")


# Programa principal
if __name__ == '__main__':
    print("CLASE ROBOT")
    url1 = Robot('http://gsyc.urjc.es/')
    url2 = Robot('https://www.aulavirtual.urjc.es')
    url1.content()
    url1.retrieve()
    url2.retrieve()
    url2.content()
    url1.show()

    print("CLASE CACHE")
    u1 = Cache()
    u2 = Cache()
    u1.retrieve('https://www.aulavirtual.urjc.es')
    u2.show('http://gsyc.urjc.es/')
    u1.show_all()
    u2.show_all()

